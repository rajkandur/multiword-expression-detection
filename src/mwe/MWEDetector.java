package mwe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import edu.stanford.nlp.util.StringUtils;

import parsing.ioparser.MWECorporaFileReader;
import parsing.ioparser.MWECorporaFileReader.CORPUSNAME;
import parsing.ioparser.MWECorporaFileReader.NGRAM;
import parsing.nlpparser.OpenNLPWrapParser;

public class MWEDetector {

	private MWECorporaFileReader m_encorporareader = null;
	private PatternMatcher m_PatternMatcher = null;
	private String[] m_srcText = null;
	String[] m_POStagarray = null;

	public void init(String text){

		if(null == m_encorporareader)
			m_encorporareader = new MWECorporaFileReader();

		try {
			//load corpus, index the parallel corpus and create index
			m_encorporareader.loadCorpus();

			// check against the standard pattern of POS
			if(m_PatternMatcher == null)
				m_PatternMatcher = new PatternMatcher();

			text = text.replaceFirst("\\.", "");
			text = text.replaceAll("'", " '");
			//text = text.replaceAll("[\\(/)-\"]", "");
			m_srcText = text.split("[ ,]");

			// tokenize the sentence and identify the POS
			OpenNLPWrapParser parser = OpenNLPWrapParser.getInstance();
			m_POStagarray = parser.POStagSentence(parser.tokenizeSentence(text));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Map<String,List<String>> tokenizeAndIdentifyPattern(){
		if(m_PatternMatcher != null)
			return m_PatternMatcher.matchPattern(m_POStagarray);

		return null;
	}

	public List<Double> validateMWE(String collocationWords, CORPUSNAME eCorpName){
		String[] arr = collocationWords.split(" ");

		List<Double> statvalues = new ArrayList<Double>();

		// two words mwe
		if(arr.length == 2){
			Map<String, Double> stats = new HashMap<String, Double>();
			stats = calculatengramCounts(arr, "first", eCorpName);
			statvalues.add(stats.get("PMI"));
			statvalues.add(stats.get("CHI"));
			statvalues.add(stats.get("LOGL"));

			//			System.out.println("=======================================");
			//			System.out.println("Statistics for \"" + collocationWords + "\"");
			//			System.out.println("=======================================");
			//			System.out.println("PMI: " + stats.get("PMI"));
			//			System.out.println("CHI: " + stats.get("CHI"));
			//			System.out.println("LOGL: " + stats.get("LOGL"));
		}

		// three words mwe
		else if(arr.length == 3){
			Map<String, Double> stats1 = new HashMap<String, Double>();
			Map<String, Double> stats2 = new HashMap<String, Double>();

			//first representation
			String[] newarr = split(NGRAM.TRIGRAM, collocationWords, "first");
			System.out.println("=============FIRST======================");
			stats1 = calculatengramCounts(newarr, "first", eCorpName);			
			//			System.out.println("Statistics for \"" + collocationWords + "\"");
			//			System.out.println("--------------------------");
			//			System.out.println("PMI: " + stats1.get("PMI"));
			//			System.out.println("CHI: " + stats1.get("CHI"));
			//			System.out.println("LOGL: " + stats1.get("LOGL"));
			//			System.out.println("---------------------");
			//			System.out.println("=======================================");

			//second representation
			newarr = split(NGRAM.TRIGRAM, collocationWords, "second");
			//			System.out.println("=============SECOND====================");
			stats2 = calculatengramCounts(newarr, "second", eCorpName);
			//			System.out.println("---------------------------");
			//			System.out.println("PMI: " + stats2.get("PMI"));
			//			System.out.println("CHI: " + stats2.get("CHI"));
			//			System.out.println("LOGL: " + stats2.get("LOGL"));
			//			System.out.println("---------------------");
			//			System.out.println("=======================================");

			//compare between them and send the highest values
			double PMI = stats1.get("PMI") >= stats2.get("PMI") ? stats1.get("PMI"): stats2.get("PMI");
			double CHI = stats1.get("CHI") >= stats2.get("CHI") ? stats1.get("CHI"): stats2.get("CHI");
			double LOGL = stats1.get("LOGL") >= stats2.get("LOGL") ? stats1.get("LOGL"): stats2.get("LOGL");

			statvalues.add(PMI);
			statvalues.add(CHI);
			statvalues.add(LOGL);
		}

		// four words mwe
		else if(arr.length == 4){
			Map<String, Double> stats1 = new HashMap<String, Double>();
			Map<String, Double> stats2 = new HashMap<String, Double>();
			Map<String, Double> stats3 = new HashMap<String, Double>();

			//first representation
			String[] newarr = split(NGRAM.FOURGRAM, collocationWords, "first");
			System.out.println("=============FIRST======================");
			stats1 = calculatengramCounts(newarr, "first", eCorpName);
			//			System.out.println("Statistics for \"" + collocationWords + "\"");
			//			System.out.println("--------------------------");
			//			System.out.println("PMI: " + stats1.get("PMI"));
			//			System.out.println("CHI: " + stats1.get("CHI"));
			//			System.out.println("LOGL: " + stats1.get("LOGL"));
			//			System.out.println("---------------------");	
			//			System.out.println("=======================================");

			//second representation
			newarr = split(NGRAM.FOURGRAM, collocationWords, "second");
			System.out.println("=============SECOND=====================");
			stats2 = calculatengramCounts(newarr, "second", eCorpName);
			//			System.out.println("---------------------------");
			//			System.out.println("PMI: " + stats2.get("PMI"));
			//			System.out.println("CHI: " + stats2.get("CHI"));
			//			System.out.println("LOGL: " + stats2.get("LOGL"));
			//			System.out.println("---------------------");	
			//			System.out.println("=======================================");

			//third representation
			newarr = split(NGRAM.FOURGRAM, collocationWords, "third");
			//			System.out.println("=============THIRD======================");
			stats3 = calculatengramCounts(newarr, "third", eCorpName);
			//			System.out.println("---------------------------");
			//			System.out.println("PMI: " + stats3.get("PMI"));
			//			System.out.println("CHI: " + stats3.get("CHI"));
			//			System.out.println("LOGL: " + stats3.get("LOGL"));
			//			System.out.println("---------------------");	
			//			System.out.println("=======================================");

			//compare between them and send the highest values
			double PMI = stats1.get("PMI") >= stats2.get("PMI") ? 
					(stats1.get("PMI") >= stats3.get("PMI") ? stats1.get("PMI"): stats3.get("PMI")):
						(stats2.get("PMI") >= stats3.get("PMI") ? stats2.get("PMI"): stats3.get("PMI"));

					double CHI = stats1.get("CHI") >= stats2.get("CHI") ? 
							(stats1.get("CHI") >= stats3.get("CHI") ? stats1.get("CHI"): stats3.get("CHI")):
								(stats2.get("CHI") >= stats3.get("CHI") ? stats2.get("CHI"): stats3.get("CHI"));

							double LOGL = stats1.get("LOGL") >= stats2.get("LOGL") ? 
									(stats1.get("LOGL") >= stats3.get("LOGL") ? stats1.get("LOGL"): stats3.get("LOGL")):
										(stats2.get("LOGL") >= stats3.get("LOGL") ? stats2.get("LOGL"): stats3.get("LOGL"));

									statvalues.add(PMI);
									statvalues.add(CHI);
									statvalues.add(LOGL);
		}		
		return statvalues;
	}

	/**
	 * 
	 * @param mwe
	 * @return
	 */
	private Map<String, Double> calculatengramCounts(String[] mwe, String representation, CORPUSNAME eCorpName)
	{
		Map<String, Double> mweCountMap = new HashMap<String, Double>();

		String ph1 = mwe[0];
		String ph2 = mwe[1];

		//create a query and search in the corpus:ph1ph2
		List<String> list = new ArrayList<String>();
		//ph1 count
		String field = new String();
		int len = ph1.split(" ").length;
		if( len == 1 || len == 2 )
			field = "Bigram";
		else
			field = "Trigram";

		list.add(field + ":\"" + ph1 + "\"");
		TopDocs ph1Docs = m_encorporareader.searchCorpus(list, eCorpName);
		int ph1Count = 0;
		if(len ==1)
			ph1Count = ph1Docs.totalHits/2;
		else
			ph1Count = ph1Docs.totalHits;

		//ph2 count
		list.clear();
		len = ph2.split(" ").length;
		if( len == 1 || len == 2 )
			field = "Bigram";
		else
			field = "Trigram";
		list.add(field + ":\"" + ph2 + "\"");
		TopDocs ph2Docs = m_encorporareader.searchCorpus(list, eCorpName);
		int ph2Count = 0;
		if(len ==1)
			ph2Count = ph2Docs.totalHits/2;
		else
			ph2Count = ph2Docs.totalHits;		

		//ph1ph2 count
		len = StringUtils.join(mwe, " ").split(" ").length;
		NGRAM eNgram = NGRAM.BIGRAM;
		if( len == 1 || len == 2 )
			field = "Bigram";
		else if( len == 3){
			field = "Trigram";
			eNgram = NGRAM.TRIGRAM;
		}
		else if( len == 4){
			field = "Fourgram";
			eNgram = NGRAM.FOURGRAM;
		}
		list.clear();
		list.add(field + ":\"" + ph1 + " " + ph2 + "\"");
		TopDocs ph1ph2Docs = m_encorporareader.searchCorpus(list, eCorpName);
		int ph1ph2Count = ph1ph2Docs.totalHits;

		//ph1~ph2 count
		list.clear();
		list.add(field + ":(\"" + ph1 + "\" " + "-\"" + ph2 + "\")");
		TopDocs ph1notph2Docs = m_encorporareader.searchCorpus(list, eCorpName);
		int ph1notph2Count = 0;
		if(ph1notph2Docs != null){
			for(ScoreDoc scoreDoc : ph1notph2Docs.scoreDocs){
				Document docen;
				try {
					docen = m_encorporareader.getM_IndexSearcher().doc(scoreDoc.doc);
					String[] text = split(eNgram, docen.get(field), representation);
					if( text != null && text.length == 2 && text[0].trim().contentEquals(ph1) && !text[1].trim().contentEquals(ph2) )
						ph1notph2Count++;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		//ph1~ph2 count
		list.clear();
		list.add(field + ":(\"" +	ph2 + "\" " + "-\"" + ph1 + "\")");
		TopDocs notph1ph2Docs = m_encorporareader.searchCorpus(list, eCorpName);
		int notph1ph2Count = 0;
		if(notph1ph2Docs != null){
			for(ScoreDoc scoreDoc : notph1ph2Docs.scoreDocs){
				Document docen;
				try {
					docen = m_encorporareader.getM_IndexSearcher().doc(scoreDoc.doc);
					String[] text = split(eNgram, docen.get(field), representation);
					if( text != null && text.length == 2 && !text[0].trim().contentEquals(ph1) && text[1].trim().contentEquals(ph2) )
						notph1ph2Count++;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		//~ph1~ph2 count
		int notph1notph2Count = 0;
		int n = m_encorporareader.getTOTALBIGRAMCOUNT();
		if(m_encorporareader != null)
			notph1notph2Count = m_encorporareader.getTOTALBIGRAMCOUNT() - (ph1ph2Count+ph1notph2Count+notph1ph2Count);

		//use PMI, chi-square(good in large corpus), log-likelihood ratio(good in small corpus)
		StatisticalAnalyzer stat =  new StatisticalAnalyzer();
		System.out.println("-------------------------------------------");
		double pmi = stat.PMI(ph1ph2Count, ph1Count, ph2Count, n); 
		//System.out.println("pmi for \"" + ph1 + " " + ph2 + "\" : " + pmi);
		System.out.println("-------------------------------------------");

		double chiS2 = stat.chisquare(ph1ph2Count, ph1notph2Count, notph1ph2Count, notph1notph2Count);
		//System.out.println("chisquare for \"" + ph1 + " " + ph2 + "\" : " + chiS2);
		System.out.println("-------------------------------------------");

		double logL = stat.loglikelihoodratio(ph1ph2Count, ph1notph2Count, notph1ph2Count, notph1notph2Count);
		//System.out.println("loglikelihood for \"" + ph1 + " " + ph2 + "\" : " + logL);
		System.out.println("-------------------------------------------");

		//creat the map
		mweCountMap.put("PMI", pmi);
		mweCountMap.put("CHI", chiS2);
		mweCountMap.put("LOGL", logL);
		return mweCountMap;
	}

	public String identifyMatchedString(String pattern){
		if(null == m_PatternMatcher)
			return null;

		String ipPOS = StringUtils.join(Arrays.asList(m_POStagarray), ";");
		String[] startendoffsets = m_PatternMatcher.identifyMatchedOffset(ipPOS, pattern).split("_");
		System.out.println(Arrays.asList(startendoffsets));
		if(startendoffsets != null){
			int nFrom =  Integer.parseInt(startendoffsets[0].trim());
			int nTo =  Integer.parseInt(startendoffsets[1].trim());
			String match = new String();
			for(int i = nFrom; i <= nTo; i++){
				if(i <= m_srcText.length)
					match += m_srcText[i];
				if(i != nTo)
					match += " ";
			}

			return match;
		}
		return null;
	}

	private String[] split(MWECorporaFileReader.NGRAM eNgram, String data, String representation){

		String[] text = new String[2];
		String[] arr = data.split(" ");

		switch (eNgram) {
		case BIGRAM:
			text = data.split(" ");
			break;
		case TRIGRAM:
			if(arr.length < 3)
				return null;

			if("first" == representation){
				text[0] = arr[0] + " " + arr[1];
				text[1] = arr[2];
			}
			else if("second" == representation){
				text[0] = arr[0];
				text[1] = arr[1] + " " + arr[2];
			}
			break;
		case FOURGRAM:

			if(arr.length < 4)
				return null;

			if("first" == representation){
				text[0] = arr[0] + " " + arr[1] + " " + arr[2];
				text[1] = arr[3];
			}
			else if("second" == representation){
				text[0] = arr[0];
				text[1] = arr[1] + " " + arr[2] + " " + arr[3];
			}
			else if("third" == representation){
				text[0] = arr[0] + " " + arr[1];
				text[1] = arr[2] + " " + arr[3];
			}
			break;
		}
		return text;
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir"));

		MWEDetector detect = new MWEDetector();
		//String sent = "His robe is also the default area for his personal time flow, though he can of course expand it to bring other things along with him when he travels.";
		//String sent = "In 1976, upon retiring from playing, Roy became an assistant coach for Chicago Sting.";
		//String sent = "assistant coach";
		//String sent = "may be you just held your tongue.";
		//String sent = "pain in the neck";
		//String sent = "We are barely pulling a knot";
		//String sent = "Investigators such as James Miller and Clement Smith carried out clinical observations";
		//String sent = "A person with a master 's degree in physics chemistry math or English yet who has not taken Education courses is not permitted to teach in the public schools said Grover.";
		//String sent = "UN General Assembly as to NATO members votes since a new ad hoc NATO committee has been set up";
		//String sent = "mother's conversation";
		//String sent = "master's degree";
		//String sent = "took the slightest exception";
		//String sent = "turn into";
		//String sent =  "University College London";
		//String sent = "(delhi boundary limits\"";
		String sent = "United States";

		detect.init(sent);
		Map<String, List<String>> map = detect.tokenizeAndIdentifyPattern();

		if(map != null){

			//validate these mwe in corpus
			//co-occurrence conditions, search corpus
			Collection<List<String>> col = map.values(); 
			Iterator<List<String>> itr = col.iterator();

			while(itr.hasNext()){

				List<String> patterns = itr.next();
				for(int i1 = 0; i1 < patterns.size(); i1++){

					//identify the offset and get the source matched string
					System.out.println(patterns.get(i1));
					String match = detect.identifyMatchedString(patterns.get(i1));
					System.out.println(match);

					//validate against wiki50 or semcor corpus
					List<Double> statvalues = detect.validateMWE(match, CORPUSNAME.WIKI50);
					System.out.println(statvalues);
					//detect.validateMWE(match, CORPUSNAME.SEMCOR3_0);
				}
			}
		}
	}

}
