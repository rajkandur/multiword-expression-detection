package mwe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import edu.stanford.nlp.util.StringUtils;

public class PatternMatcher {

	private Map<String, String> m_MWEPatternAndRegex = null;
	private List<String> m_MWEPATTERN = null;

	public PatternMatcher() {
		
		//From: Table 1. Patterns of Morphological Inflection - Arranz et al., 2005
		//standard nominal mw pattern
		if(m_MWEPATTERN == null)
			m_MWEPATTERN = new ArrayList<String>();
		
		//standard verbal mw pattern
		m_MWEPATTERN.add("[NN],NN?,NN?,[NN]"); //updated from NN,NN?,NN?,[NN] to [NN],NN?,NN?,[NN]
		m_MWEPATTERN.add("[NN],IN,DT?,NN");
		m_MWEPATTERN.add("NN,POS,[NN]");
		m_MWEPATTERN.add("JJ+,[NN]"); //updated from JJ,[NN] to JJ+,[NN]
		m_MWEPATTERN.add("[NN],IN,JJ,[NN]");
		m_MWEPATTERN.add("IN,[NN]");
		m_MWEPATTERN.add("[NN],CC,[NN]");

		//verb related
		m_MWEPATTERN.add("[VB],IN,(RB|IN)?,(RB|IN)?");
		m_MWEPATTERN.add("[VB],RB,(RB|IN)?,(RB|IN)?");
		m_MWEPATTERN.add("[VB],TO,VB");
		m_MWEPATTERN.add("[VB],IN?,(DT|PRP\\$)?,[NN]");
		
		//new patterns manually checked and added
		m_MWEPATTERN.add("[VB],RP"); //try attending send it out, call kim up etc.,
		m_MWEPATTERN.add("[VB],DT,[JJ],NN");
		m_MWEPATTERN.add("[VB],RB?,JJ+");
		
		//convert all the patterns to regex
		if(m_MWEPatternAndRegex == null)
			m_MWEPatternAndRegex = new HashMap<String, String>();
		for(int i = 0; i < m_MWEPATTERN.size(); i++)
			m_MWEPatternAndRegex.put(m_MWEPATTERN.get(i), convertToRegex(m_MWEPATTERN.get(i)));
	}

	public String convertToRegex(String mwepattern){

		//split into array
		String[] patternArray = mwepattern.split(",");

		//convert to regex
		for(int i = 0; i < patternArray.length; i++){
			//if the entity is followed by ?  => optional
			if(patternArray[i].endsWith("?")){ patternArray[i] = "(" + patternArray[i].trim().replace("?", ")?;?"/*"){0,1}"*/);}

			//if the entity is followed by +  => optional
			if(patternArray[i].endsWith("+")){ patternArray[i] = "(" + patternArray[i].trim().replace("+", ";?)+"/*"){0,1}"*/);}

			//if the entity is in square bracket => can undergo inflections
			else if(patternArray[i].endsWith("]") && patternArray[i].startsWith("[")){
				patternArray[i] = patternArray[i].trim().replace("[", "(").replace("]", "[A-Za-z]?);?");}

			else patternArray[i] = patternArray[i].trim() + ";?";
		}

		String regex = StringUtils.join(patternArray, "");
		String regexw = "(;?" + regex + ")";
		System.out.println(regexw);
		return regexw;
	}

	public Map<String, List<String>> matchPattern(String[] input){

		//check for null and check for empty
		if( null == m_MWEPatternAndRegex || m_MWEPatternAndRegex.isEmpty()) return null;

		//get the patterns
		Set<String> patternList = m_MWEPatternAndRegex.keySet();

		//trim the input text pos tags
		for(int i = 0;i < input.length; i++){
			input[i] = input[i].trim();
		}

		String iptext = StringUtils.join(Arrays.asList(input), ";");
		iptext = iptext.replaceFirst("\\.", "");
		System.out.println(iptext);

		//find the pattern in the input text(PartOfSpeech list)
		//compare the pattern with input and see whether there is a match
		Map<String, List<String>> matchedMWEList = new HashMap<String, List<String>>();
		Iterator<String> itr1 = patternList.iterator();
		while(itr1.hasNext()){
			//get the regex string patterns
			String pat = itr1.next();
			String regexpat = m_MWEPatternAndRegex.get(pat);

			//create a pattern object from the regex
			Pattern pattern = Pattern.compile(regexpat);

			//get the matched pattern if present
			List<String> matchedList = StringUtils.regexGroups(pattern, iptext);

			if(matchedList != null){
				//remove null and single entities
				Iterator<String> itr2 = matchedList.iterator();
				while(itr2.hasNext()){
					String val = itr2.next();

					if(val == null || val.split(";").length == 1)
						itr2.remove();
				}

				//from the pattern, identify the text
				matchedMWEList.put(pat, matchedList);
//				System.out.println(matchedList);
//				System.out.println("------------------------------------------");
			}
		}

		return matchedMWEList;
	}

	/**
	 * 
	 * @param text : Source 
	 * @param pattern : Pattern found in the source
	 * @return startoffset_endoffset (offsets are word positions in array. NOT CHARACTER positions)
	 */
	public String identifyMatchedOffset(String text, String pattern){

		//remove initial and final delimiter
		text = text.replaceFirst("^;", "").replaceFirst("\\.$", "").replaceFirst(";$", "");
		pattern = pattern.replaceFirst("^;", "").replaceFirst("\\.$", "").replaceFirst(";$", "");
		
		String[] iparray = text.split(";");
		String[] matchedarray = pattern.split(";");

		//calculating the length
		int length = iparray.length;
		if(iparray.length-matchedarray.length > 0)
			length = iparray.length-matchedarray.length;
		
		//getting the offset for the matched string
		for(int i = 0; i <= length; i++){
			int k = i;
			for(int j = 0; j < matchedarray.length; j++){
				if(iparray[k++].contains(matchedarray[j])){
					if( matchedarray.length-1 == j)
						return String.valueOf(i) + "_" + String.valueOf(i+matchedarray.length-1);
					else
						continue;
				}
				else 
					break;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		PatternMatcher matcher = new PatternMatcher();
		String offset = matcher.identifyMatchedOffset(";His;Robe;is;also;the;", ";is;also;");
		System.out.println(offset);
	}
}
