package ml;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.AbstractFileLoader;
import weka.core.converters.AbstractFileSaver;
import weka.core.converters.ConverterUtils;

public class MWEClassifier {

	private String m_arffPath = null;
	private Instances m_ins = null;
	private Classifier m_dtree = null;

	public MWEClassifier() {
		m_arffPath = new String();
	}

	public String getM_arffPath() {
		return m_arffPath;
	}

	/***
	 * create arff from csv
	 * @param path
	 */
	boolean prepareData(String csvpath, String newarffpath){

		File file = new File(csvpath);

		//check the file existence		
		if(!file.exists()) return false;

		//get the loader for csv
		AbstractFileLoader loader = ConverterUtils.getLoaderForFile(file);	
		if(null == loader)
			return false;

		try {
			loader.setSource(file);
			Instances ins = loader.getDataSet();

			//set the class index
			if (ins.classIndex() == -1)
				ins.setClassIndex(ins.numAttributes() - 1);

			//get the saver for arff
			AbstractFileSaver arffsaver = ConverterUtils.getSaverForExtension(".arff");
			if(null == arffsaver)
				return false;

			//save into arff
			File newfile = new File(newarffpath);
			arffsaver.setFile(newfile);
			arffsaver.setInstances(ins);
			arffsaver.writeBatch();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * train the model
	 */
	boolean buildClassifier(String arffpath){

		m_arffPath = arffpath;
		File file = new File(m_arffPath);

		//get the loader for csv
		AbstractFileLoader loader = ConverterUtils.getLoaderForFile(file);	
		if(null == loader)
			return false;

		try {

			loader.setSource(file);
			m_ins = loader.getDataSet();

			//set the class index
			if (m_ins.classIndex() == -1)
				m_ins.setClassIndex(m_ins.numAttributes() - 1);

			String[] options = new String[1];
			options[0] = "-U"; //unpruned tree

			//there are no instances
			if(null == m_ins)
				return false;

			//create a decision tree
			m_dtree = new J48();
			m_dtree.setOptions(options);
			m_dtree.buildClassifier(m_ins);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Validating the gold standards
	 * Note: The classifier should not be trained while calling cross-validation. (here am using classifier string)
	 */
	void crossfoldvalidate(){
		try {

			if(null == m_ins)
				System.out.println("Load the model before doing cross-validation.");

			String[] options = new String[1];
			options[0] = "-U"; //unpruned tree

			Evaluation eval = new Evaluation(m_ins);
			eval.crossValidateModel(new J48(), m_ins, 10, new Random(1));
			System.out.println(eval.toSummaryString("\nResults\n======\n", true));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MWEClassifier c = new MWEClassifier();
		c.prepareData("./total_data_wiki50.csv", "./total_data_wiki50.arff");
		c.buildClassifier("./total_data_wiki50.arff");
		c.crossfoldvalidate();
	}
}
