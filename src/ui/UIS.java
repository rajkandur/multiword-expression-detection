package ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


public class UIS {

	private JFrame frmMweDetection;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIS window = new UIS();
					window.frmMweDetection.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UIS() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMweDetection = new JFrame();
		frmMweDetection.setTitle("MWE detection");
		frmMweDetection.setBounds(100, 100, 600, 400);
		frmMweDetection.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		frmMweDetection.getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblInput = new JLabel("Input:");
		lblInput.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblInput);
		
		JTextArea textArea = new JTextArea();
		textArea.setRows(7);
		textArea.setColumns(30);
		panel.add(textArea);
		
		JButton btnFindMwe = new JButton("Find MWE");
		panel.add(btnFindMwe);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmMweDetection.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_1.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		tabbedPane.addTab("Patterns", null, panel_1, null);
		
		JLabel lblPatternsFound = new JLabel("Patterns Found:");
		lblPatternsFound.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(lblPatternsFound);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setRows(10);
		textArea_1.setColumns(40);
		panel_1.add(textArea_1);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_2.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		tabbedPane.addTab("Statistical analysis", null, panel_2, null);
		
		JLabel lblNewLabel = new JLabel("Statistical values obtained from the given input:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(lblNewLabel);
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3);
		
		JLabel lblPmi = new JLabel("PMI:");
		panel_3.add(lblPmi);
		
		textField = new JTextField();
		panel_3.add(textField);
		textField.setColumns(9);
		
		JLabel lblNewLabel_1 = new JLabel("Chi-Square");
		panel_3.add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		panel_3.add(textField_1);
		textField_1.setColumns(9);
		
		JLabel lblLogliklihood = new JLabel("Log-Likelihood:");
		panel_3.add(lblLogliklihood);
		
		textField_2 = new JTextField();
		panel_3.add(textField_2);
		textField_2.setColumns(9);
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		
		JLabel lblGoldStandards = new JLabel("Gold Standards:");
		panel_4.add(lblGoldStandards);
		
		JPanel panel_5 = new JPanel();
		panel_2.add(panel_5);
		
		JLabel lblPmi_1 = new JLabel("PMI:");
		panel_5.add(lblPmi_1);
		
		textField_3 = new JTextField();
		panel_5.add(textField_3);
		textField_3.setColumns(9);
		
		JLabel lblChisquare = new JLabel("Chi-Square:");
		panel_5.add(lblChisquare);
		
		textField_4 = new JTextField();
		panel_5.add(textField_4);
		textField_4.setColumns(9);
		
		JLabel lblLogliklihood_1 = new JLabel("Log-Likelihood:");
		panel_5.add(lblLogliklihood_1);
		
		textField_5 = new JTextField();
		panel_5.add(textField_5);
		textField_5.setColumns(9);
	}

}
