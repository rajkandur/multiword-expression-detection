package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mwe.MWEDetector;

import parsing.ioparser.MWECorporaFileReader.CORPUSNAME;
import parsing.nlpparser.OpenNLPWrapParser;


public class readfilesemcor
{
	public void readfile()
	{
		File file = new File("./split/semcor2/semcor.txt");
		if(!file.exists()) return;
		
		File filew = new File("./split/semcor2/solution/split16sol.csv");
		//File filew = new File("./split/solution/split1sol.txt");
		//if(!filew.exists()) return;
		
		

		//create corpus readers
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			BufferedWriter bw = new BufferedWriter(new FileWriter(filew));
			
			
			String line;
			String name = null;
			String word = null;
			int count =0;
			OpenNLPWrapParser op = OpenNLPWrapParser.getInstance();
			while ((line = br.readLine()) != null) 
			{
				int ch = line.indexOf(',');
				if(true)
				{
					//name = line.substring(0, ch);
					word = line;
					if(true)
					{
						//System.out.println("name:"+name+":");
						System.out.println("word:"+word+":");
						System.out.println("begin"+line+"end");
						count++;
						String[] postags = op.POStagSentence(op.tokenizeSentence(word));
						String pos = null;
						if(postags.length!=0)
							pos = postags[0];
						for(int i = 1; i< postags.length; i++)
						{
							pos = pos+","+postags[i];
						}
						System.out.println("tags"+pos);
						
						System.out.println(System.getProperty("user.dir"));

						MWEDetector detect = new MWEDetector();
						detect.init(word);
						Map<String, List<String>> map = detect.tokenizeAndIdentifyPattern();
						if(map != null)
						{

							//validate these mwe in corpus
							//co-occurrence conditions, search corpus
							Collection<List<String>> col = map.values(); 
							Iterator<List<String>> itr = col.iterator();

							while(itr.hasNext())
							{

								List<String> patterns = itr.next();
								for(int i1 = 0; i1 < patterns.size(); i1++)
								{

									//identify the offset and get the source matched string
									System.out.println(patterns.get(i1));
									String match = detect.identifyMatchedString(patterns.get(i1));
									System.out.println(match);
									List<Double> statvalues = detect.validateMWE(match, CORPUSNAME.SEMCOR3_0);
									System.out.println(statvalues);
									if(!Double.isNaN(statvalues.get(0))&&!Double.isInfinite(statvalues.get(0))&&!Double.isNaN(statvalues.get(1))&&!Double.isInfinite(statvalues.get(1))&&!Double.isNaN(statvalues.get(2))&&!Double.isInfinite(statvalues.get(2)))
									{
										bw.append(word+", " + patterns.get(i1) + ", " + statvalues.get(0)+ ", " + statvalues.get(1)+ ", " + statvalues.get(2));
										//bw.write(word+", \"" + pos + "\", " + match + ", " + statvalues);
										bw.newLine();
										//bw.write("new, line");
									}
								}
							}
						}

					}
				}
			}
			br.close();
			bw.close();
			
			/*
			 * 
			 * public String[] POStagSentence(String[] sent){
		return m_postagger.tag(sent);
	}
			 */
			
			
			System.out.println("count:"+count);
			
			//System.out.println("begin"+countdata+"end");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String args[])
	{
		readfilesemcor rf = new readfilesemcor();
		rf.readfile();
	}
	

}
