package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mwe.MWEDetector;

import parsing.ioparser.MWECorporaFileReader.CORPUSNAME;
import parsing.nlpparser.OpenNLPWrapParser;


public class negativeExamples
{
	public void readfile()
	{
		File file2 = new File("./split/mwe_wiki501.csv");
		if(!file2.exists()) return;
		File file = new File("./split/semcor/text7.txt");
		if(!file.exists()) return;
		
		File filew = new File("./split/semcor/sol/text7sol.csv");
		//File filew = new File("./split/solution/split1sol.txt");
		//if(!filew.exists()) return;
		
		

		//create corpus readers
		try {
			BufferedReader br2 = new BufferedReader(new FileReader(file2));
			String line2;
			String name2 = null;
			String word2 = null;
			ArrayList<String> mwelist = new ArrayList<String>();
			while ((line2 = br2.readLine()) != null) 
			{
				int ch = line2.indexOf(',');
				if(ch!=-1)
				{
					name2 = line2.substring(0, ch);
					word2 = line2.substring(ch+1, line2.length()-1);
						System.out.println("name:"+name2+":");
						System.out.println("word:"+word2+":");
						mwelist.add(word2);

				}
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			BufferedWriter bw = new BufferedWriter(new FileWriter(filew));
			
			
			String line;
			String name = null;
			String word = null;
			int count =0;
			OpenNLPWrapParser op = OpenNLPWrapParser.getInstance();
			while ((line = br.readLine()) != null) 
			{
					
						String[] postags = op.POStagSentence(op.tokenizeSentence(line));
						/*String str1 = "Bacteriological water analysis is a method of analysing water to estimate the numbers of bacteria present and, if needed, to find out what sort of bacteria they are. It is a microbiological analytical procedure which uses samples of water and from these samples determines the concentration of bacteria. It is then possible to draw inferences about the suitability of the water for use from these concentrations. This process is used, for example, to routinely confirm that water is safe for human consumption or that bathing and recreational waters are safe to use."
+"The interpretation and the action trigger levels for different waters vary depending on the use made of the water. Very stringent levels applying to drinking water whilst more relaxed levels apply to marine bathing waters where much lower volumes of water are expected to be ingested by users."
+"The common feature of all these routine screening procedures is that the primary analysis is for indicator organisms rather than the pathogens that might cause concern. Indicator organisms are bacteria such as nonspecific coliforms, Escherichia coli and Pseudomonas aeruginosa that are very commonly found in the human or animal gut and which, if detected, may suggest the presence of sewage. Indicator organisms are used because even when a person is infected with a more pathogenic bacteria, they will still be excreting many millions times more indicator organisms than pathogens. It is therefore reasonable to surmise that if indicator organism levels are low, then pathogen levels will be very much lower or absent. Judgements as to suitability of water for use are based on very extensive precedents and relate to the probability of any sample population of bacteria being able to be infective at a reasonable statistical level of confidence."
+"Analysis is usually performed using culture, biochemical and sometimes optical methods. When indicator organisms levels exceed preset triggers, specific analysis for pathogens may then be undertaken and these can be quickly detected where suspected using specific culture methods or molecular biology."
+"Because the analysis is always based on a very small sample taken from a very large volume of water,";*/
						String pos = null;
						if(postags.length!=0)
							pos = postags[0];
						for(int i = 1; i< postags.length; i++)
						{
							pos = pos+","+postags[i];
						}
						System.out.println("tags"+pos);
						
						System.out.println(System.getProperty("user.dir"));

						MWEDetector detect = new MWEDetector();
						detect.init(line);
						Map<String, List<String>> map = detect.tokenizeAndIdentifyPattern();
						if(map != null)
						{

							//validate these mwe in corpus
							//co-occurrence conditions, search corpus
							Collection<List<String>> col = map.values(); 
							Iterator<List<String>> itr = col.iterator();

							while(itr.hasNext())
							{

								List<String> patterns = itr.next();
								for(int i1 = 0; i1 < patterns.size(); i1++)
								{

									//identify the offset and get the source matched string
									System.out.println(patterns.get(i1));
									String match = detect.identifyMatchedString(patterns.get(i1));
									System.out.println("match"+match);
									List<Double> statvalues = detect.validateMWE(match, CORPUSNAME.WIKI50);
									System.out.println(statvalues);
									if(!Double.isNaN(statvalues.get(0))&&!Double.isInfinite(statvalues.get(0))&&!Double.isNaN(statvalues.get(1))&&!Double.isInfinite(statvalues.get(1))&&!Double.isNaN(statvalues.get(2))&&!Double.isInfinite(statvalues.get(2)))
									{
										if(!mwelist.contains(match))
										{bw.append(match+", " + patterns.get(i1) + ", " + statvalues.get(0)+ ", " + statvalues.get(1)+ ", " + statvalues.get(2));
										//bw.write(word+", \"" + pos + "\", " + match + ", " + statvalues);
										bw.newLine();
										}
										//bw.write("new, line");
									}
								}
							}
						

					
				}
			}
			br.close();
			bw.close();
			
			/*
			 * 
			 * public String[] POStagSentence(String[] sent){
		return m_postagger.tag(sent);
	}
			 */
			
			
			System.out.println("count:"+count);
			
			//System.out.println("begin"+countdata+"end");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String args[])
	{
		negativeExamples rf = new negativeExamples();
		rf.readfile();
	}
	

}
