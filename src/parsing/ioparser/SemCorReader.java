package parsing.ioparser;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import edu.mit.jsemcor.element.IContext;
import edu.mit.jsemcor.element.ISentence;
import edu.mit.jsemcor.element.IWordform;
import edu.mit.jsemcor.main.IConcordance;
import edu.mit.jsemcor.main.IConcordanceSet;
import edu.mit.jsemcor.main.Semcor;

public class SemCorReader {

	/**
	 * semcor relative path
	 */
	private String SEMCORDIR = "./corpora/semcor3.0";

	public SemCorReader() {
		
	}
		
	/**
	 * 
	 * @return
	 */
	public String getSEMCORDIR() {
		return SEMCORDIR;
	}
	/**
	 * 
	 * @param sEMCORDIR
	 */
	public void setSEMCORDIR(String sEMCORDIR) {
		SEMCORDIR = sEMCORDIR;
	}
	
	/**
	 * Loads all the files of semcors
	 * @param dir
	 * @return
	 */
	public List<String> getSemcorSentences()
	{
		List<String> semCorSentences = new ArrayList<String>();
		URL url = null;
		try {

			//create a url to semcor
			url = new URL("file", null, SEMCORDIR);
			IConcordanceSet semcor = new Semcor(url);

			//load semcor subdir brown1, brown2 and brownv
			if(!semcor.open())
				return null;
			
			File dir = new File(SEMCORDIR);

			//list only subdirs
			File[] onlysubdirs = dir.listFiles(new OnlyDirectories());
			for (File subdir : onlysubdirs) {
				IConcordance concord = semcor.get(subdir.getName());
				List<File> files = recursivelyGetAllFilelevelpath(subdir);

				System.out.println(subdir.getName() + ": " + files.size());
				
				//get all the files
				for (File file : files) {
					IContext context = concord.getContext(file.getName());
					List<ISentence> sents = context.getSentences();

					//get all the sentences
					//System.out.println("filename" + file.getName());
					for (int i = 0; i < sents.size(); i++) {
						List<IWordform> wfl = sents.get(i).getWordList();
						List<List<IWordform>> col = sents.get(i).getCollocations();
						for (int j = 0; j < col.size(); j++) {
							for (IWordform iWordform : col.get(j)) {
								String text = iWordform.getText().replace("_", " ");
								System.out.println(text);
							}	
							System.out.println("-----------");
						}
						String s = new String();
						for (IWordform iWordform : wfl) {
							String text = iWordform.getText().replace("_", " ");
							if(iWordform.getPOSTag().getValue().equals("POS"))
								s = s.trim() + text + " ";
							else
								s += text + " ";
						}
						s = s.trim() + ".";
						semCorSentences.add(s);
					}
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(semCorSentences.get(semCorSentences.size()-1));
		return semCorSentences;
	}


	/**
	 * Lists only files
	 * @author kandur
	 *
	 */
	public static class OnlyFiles implements FileFilter{
		@Override
		public boolean accept(File file) {
			if(file.isFile())
				return true;
			return false;
		}
	}

	/**
	 * Lists only directories
	 * @author kandur
	 *
	 */
	public static class OnlyDirectories implements FileFilter{
		@Override
		public boolean accept(File file) {
			if(file.isDirectory())
				return true;
			return false;
		}
	}

	private List<File> recursivelyGetAllFilelevelpath(File dir){
		List<File> AllFiles = new ArrayList<File>();
		File[] subdirs = dir.listFiles();
		Queue<File> queue = new LinkedList<File>();
		queue.addAll(Arrays.asList(subdirs));
		
		//bfs tree traversal
		while(!queue.isEmpty()){
			File f = queue.remove();
			if(f.isDirectory())
				queue.addAll(Arrays.asList(f.listFiles()));
			else
				AllFiles.add(f);
		}
		return AllFiles;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SemCorReader reader = new SemCorReader();
		List<String> list = reader.getSemcorSentences();
		//System.out.println(list.get(list.size()-1));
	}
}
