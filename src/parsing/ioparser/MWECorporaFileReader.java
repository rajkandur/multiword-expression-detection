package parsing.ioparser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import parsing.nlpparser.OpenNLPWrapParser;

public class MWECorporaFileReader {

	//index directory for en-en corpus
	String m_wiki50indexDirectory = System.getProperty("user.dir") + "/db/wikiindex";
	String m_SemCorindexDirectory = System.getProperty("user.dir") + "/db/semcorindex";
	public static String WORDELIMITER = "\n";
	public static String SENTENCEDELIMITER = "\r\n";
	int TOTALBIGRAMCOUNT = 0;
	int TOTALTRIGRAMCOUNT = 0;
	int TOTALFOURGRAMCOUNT = 0;

	public static enum NGRAM{
		BIGRAM,
		TRIGRAM,
		FOURGRAM
	};
	
	public static enum CORPUSNAME{
		WIKI50,
		SEMCOR3_0
	};

	IndexSearcher m_IndexSearcher = null;
	SemCorReader m_semcor = null;
	Wiki50Reader m_wiki50 = null;

	public MWECorporaFileReader() {
		File file = new File("./COUNT.txt");
		if(!file.exists()) return;

		//create corpus readers
		m_semcor = new SemCorReader();
		m_wiki50 = new Wiki50Reader();

		try {
			FileInputStream in = new FileInputStream(file);
			byte[] data = new byte[in.available()];
			in.read(data);
			String countdata = new String(data);
			TOTALBIGRAMCOUNT = Integer.parseInt(countdata.split(SENTENCEDELIMITER)[0].split(":")[1].trim());
			TOTALTRIGRAMCOUNT = Integer.parseInt(countdata.split(SENTENCEDELIMITER)[1].split(":")[1].trim());
			TOTALFOURGRAMCOUNT = Integer.parseInt(countdata.split(SENTENCEDELIMITER)[2].split(":")[1].trim());

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public IndexSearcher getM_IndexSearcher() {
		return m_IndexSearcher;
	}

	/**
	 * 
	 * @param filepath
	 * @throws IOException
	 */
	public void loadCorpus() throws IOException
	{
		if(!(new File(m_wiki50indexDirectory).exists())){
			writeCorpusStoredIndexdir(CORPUSNAME.WIKI50);
		}
		
		if(!(new File(m_SemCorindexDirectory).exists())){
			writeCorpusStoredIndexdir(CORPUSNAME.SEMCOR3_0);
		}
	}

	/**
	 * 
	 * @return total number of bigrams
	 */
	public int getTOTALBIGRAMCOUNT() {
		return TOTALBIGRAMCOUNT;
	}

	/**
	 * 
	 * @return total number of trigrams
	 */
	public int getTOTALTRIGRAMCOUNT() {
		return TOTALTRIGRAMCOUNT;
	}

	/**
	 * 
	 * @return total number of fourgrams
	 */
	public int getTOTALFOURGRAMCOUNT() {
		return TOTALFOURGRAMCOUNT;
	}

	/**
	 * 
	 * @param filepath
	 * @throws IOException
	 */
	private void writeCorpusStoredIndexdir(CORPUSNAME eCorpusname) throws IOException
	{
		Analyzer analyzer = new SimpleAnalyzer(Version.LUCENE_40);

		// To store an index on disk
		Directory directory = null;
		String indexdirectory = "";

		try {

			List<String> totalSentList = new ArrayList<String>();

			if(eCorpusname == CORPUSNAME.WIKI50){
				//reading wiki50 corpus
				if(null != m_wiki50)
					totalSentList.addAll(m_wiki50.getwiki50Sentences());
				
				indexdirectory = m_wiki50indexDirectory;
			}

			if(eCorpusname == CORPUSNAME.SEMCOR3_0){
				//reading semcor corpus
				if(null != m_semcor)
					totalSentList.addAll(m_semcor.getSemcorSentences());
				
				indexdirectory = m_SemCorindexDirectory;
			}

			File indexdir = new File(indexdirectory);
			directory = SimpleFSDirectory.open(indexdir);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
			IndexWriter iwriter = new IndexWriter(directory, config);

			int nLineCount = 1;

			//get the POS for all the tokens in a line and store as it is
			OpenNLPWrapParser parser = OpenNLPWrapParser.getInstance();
			Iterator<String> itr = totalSentList.iterator();
			//File outfile = new File("./corpusloaded.txt");
			//FileOutputStream o = new FileOutputStream(outfile);
			while( itr.hasNext() )
			{
				//get the sentences
				String s = itr.next();
				
				//make POS('s) as separate token
				//s = s.replaceAll("'", " '");
				
				//o.write(s.getBytes());

				//tokenize and get POS
				String[] POStagarray = parser.POStagSentence(parser.tokenizeSentence(s));
				
				//construct bigram, trigram and fourgram texts
				List<String> bigramlist = constructngrams(s, NGRAM.BIGRAM);
				TOTALBIGRAMCOUNT += bigramlist.size();
				if(!bigramlist.isEmpty()){
					for (String bigram : bigramlist) {
						Document doc = new Document();
						doc.add(new Field("LineNumber", String.valueOf(nLineCount++), TextField.TYPE_STORED));
						doc.add(new Field("OriginalText", s, TextField.TYPE_STORED));
						doc.add(new Field("POSTagListText", StringUtils.join(POStagarray, ","), TextField.TYPE_STORED));
						doc.add(new Field("Bigram", bigram, TextField.TYPE_STORED));
						iwriter.addDocument(doc);
					}
				}		
				List<String> trigramlist = constructngrams(s, NGRAM.TRIGRAM);
				TOTALTRIGRAMCOUNT += trigramlist.size();
				if(!trigramlist.isEmpty()){
					for (String trigram : trigramlist) {
						Document doc = new Document();
						doc.add(new Field("LineNumber", String.valueOf(nLineCount++), TextField.TYPE_STORED));
						doc.add(new Field("OriginalText", s, TextField.TYPE_STORED));
						doc.add(new Field("POSTagListText", StringUtils.join(POStagarray, ","), TextField.TYPE_STORED));
						doc.add(new Field("Trigram", trigram, TextField.TYPE_STORED));
						iwriter.addDocument(doc);
					}
				}
				List<String> fourgramlist = constructngrams(s, NGRAM.FOURGRAM);
				TOTALFOURGRAMCOUNT += fourgramlist.size();
				if(!fourgramlist.isEmpty()){
					for (String fourgram : fourgramlist) {
						Document doc = new Document();
						doc.add(new Field("LineNumber", String.valueOf(nLineCount++), TextField.TYPE_STORED));
						doc.add(new Field("OriginalText", s, TextField.TYPE_STORED));
						doc.add(new Field("POSTagListText", StringUtils.join(POStagarray, ","), TextField.TYPE_STORED));
						doc.add(new Field("Fourgram", fourgram, TextField.TYPE_STORED));
						iwriter.addDocument(doc);
					}
				}

				//test whether all data is read
				if(!itr.hasNext()){
					System.out.println("------------------------");
					System.out.println(s);
					System.out.println(nLineCount);
					System.out.println("--------------------------");
				}
			}

			iwriter.close();
			//o.close();

			//write the counts into a local file
			String data = "bigram:" + TOTALBIGRAMCOUNT + SENTENCEDELIMITER;
			data += "trigram:" + TOTALTRIGRAMCOUNT + SENTENCEDELIMITER;
			data += "fourgram:" + TOTALFOURGRAMCOUNT + SENTENCEDELIMITER;
			File countFile = new File("./COUNT.txt");
			FileOutputStream out = new FileOutputStream(countFile);
			out.write(data.getBytes());
			out.close();

		} catch (IOException e) {
			e.printStackTrace();

			//remove the corrupt index directory
			if(directory != null)
				directory.deleteFile(indexdirectory);
		}
	}

	/**
	 * Does search in the stored index of the corpus
	 * @return The search results. null if there was an error.
	 */
	public TopDocs searchCorpus(List<String> collocationWords, CORPUSNAME eCorpusName) {
		try {		

			String indexdirectory = "";
			if(eCorpusName == CORPUSNAME.WIKI50) indexdirectory = m_wiki50indexDirectory;
			else if(eCorpusName == CORPUSNAME.SEMCOR3_0) indexdirectory = m_SemCorindexDirectory;
			
			System.out.println("Query: " + collocationWords);
			Directory directory = SimpleFSDirectory.open(new File(indexdirectory));
			IndexReader indexreader = DirectoryReader.open(directory);

			//performance is increased if we use only one IndexSearcher for all the searches
			if( null == m_IndexSearcher )
				m_IndexSearcher = new IndexSearcher(indexreader);

			Analyzer analyzer = new SimpleAnalyzer(Version.LUCENE_40);

			//creates the query parser object with the clause in the form of BNF
			Iterator<String> itr = collocationWords.iterator();
			String clause = null;
			while(itr.hasNext()){
				clause = itr.next();

				if(itr.hasNext())
					clause += " AND ";
			}

			QueryParser parser = new QueryParser(Version.LUCENE_40, clause, analyzer);

			//creates a query object with the criteria set by the user, if the criteria is not mentioned the previous default field is used
			Query query;
			TopDocs topdocs = null;

			//search criteria shall take inputs from the user
			query = parser.parse(clause); 

			// search get the topdocs
			topdocs = m_IndexSearcher.search(query, 10000);
			return topdocs;
		} 
		catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	List<String> constructngrams(String sentence, NGRAM engram){

		sentence = sentence.replaceAll("[,|;|\\-|.|\"]", "");
		sentence = sentence.replaceAll("  ", " ");
		String[] src = sentence.split(" ");
		List<String> ngramList = new ArrayList<String>();

		String tempf = null;
		String temps = null;
		String tempt = null;
		switch (engram) {
		case BIGRAM:
			for (int i = 0; i < src.length-1; i++) {
				String first = src[i].trim();
				String second = src[i+1].trim();

				//bracket entities should be done separately
				if(first.contentEquals("(") || second.contentEquals(")")) continue;
				else if(second.contentEquals("(")){
					tempf = first;
					continue;
				}	
				else if(first.contentEquals(")")){
					first = tempf;
					tempf = "";
				}
				ngramList.add(first + " " + second);
			}
			break;
		case TRIGRAM:
			for (int i = 0; i < src.length-2; i++) {
				String first = src[i].trim();
				String second = src[i+1].trim();
				String third = src[i+2].trim();
				//bracket entities should be done separately
				if( first.contentEquals("(") || second.contentEquals("(") ||  
						second.contentEquals(")") || third.contentEquals(")")) continue;
				else if(third.contentEquals("(")){
					tempf = first;
					temps = second;
					continue;
				}	
				else if(first.contentEquals(")")){
					ngramList.add(tempf + " " + temps + " " + second);
					first = temps;
					tempf = temps = "";
				}
				ngramList.add(first + " " + second + " " + third);
			}
			break;
		case FOURGRAM:
			for (int i = 0; i < src.length-3; i++) {
				String first = src[i].trim();
				String second = src[i+1].trim();
				String third = src[i+2].trim();
				String fourth = src[i+3].trim();
				//bracket entities should be done separately
				if( first.contentEquals("(") || second.contentEquals("(") || third.contentEquals("(") || 
						second.contentEquals(")") || third.contentEquals(")") || fourth.contentEquals(")")) continue;
				else if(fourth.contentEquals("(")){
					tempf = first;
					temps = second;
					tempt = third;
					continue;
				}	
				else if(first.contentEquals(")")){
					ngramList.add(tempf + " " + temps + " " + tempt + " " + second);
					ngramList.add(temps + " " + tempt + " " + second + " " + third);
					first = tempt;
					tempf = temps = tempt = "";
				}
				ngramList.add(first + " " + second + " " + third + " " + fourth);
			}
			break;
		default:
			return null;
		}
		return ngramList;
	}
	/**
	 * Unigram search: search only with bigram key(repeated exactly double as search word is repeated in two bigrams). => totalhits/2
	 * Bigram search: since order is not maintained in lucene search, get the hits docs and separate out as required i.e., ph1~ph2, ~ph1ph2
	 * ~ph1~ph2 => total bigram count - (ph1ph2 + ph1~ph2 + ~ph1ph2)
	 * Trigram search: two ways of representation. w1w2w3 => a) w1(w2w3)-> ph1(ph2) b) (w1w2)w3 -> (ph1)ph2 
	 * Fourgram search: three ways of representation. w1w2w3w4 => a) w1(w2w3w4)-> ph1(ph2) b) (w1w2w3)w4 -> (ph1)ph2 c) (w1w2)(w3w4) -> (ph1)(ph2)
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		MWECorporaFileReader m_encorporareader = new MWECorporaFileReader();
		m_encorporareader.loadCorpus();
		//create a query and search in the corpus
		List<String> list = new ArrayList<String>();
		//list.add("Bigram:" + "\"" + "assistant coach" + "\""); /*1*/ 
		//list.add("Bigram:" + "\"assistant\""); /*10*/
		//list.add("Bigram:" + "\"coach\""); 
		//list.add("Bigram:(assistant)"); /*25*/
		//list.add("Bigram:(\"assistant\" " + "-\"coach\")"); /*9*/
		//list.add("Bigram:( a* -\"assistant\" " + "-\"coach\")");
		list.add("Bigram:(\"as\" " + "-\"James\")"); 

		TopDocs ph1ph2Docs = m_encorporareader.searchCorpus(list, CORPUSNAME.WIKI50);

		//get the search results
		System.out.println("Search Results.");
		System.out.println("============================================");
		System.out.println("Total hits: " + ph1ph2Docs.totalHits);

		if(ph1ph2Docs != null){
			for(ScoreDoc scoreDoc : ph1ph2Docs.scoreDocs){
				Document docen;
				try {

					docen = m_encorporareader.getM_IndexSearcher().doc(scoreDoc.doc);
					System.out.println("---------------------------------------");
					System.out.println("Score of the document: " + scoreDoc.score);
					System.out.println("English Text: " + docen.get("OriginalText"));
					System.out.println("MWE Token Text: " + docen.get("MWETokenType"));
					System.out.println("POS: " + docen.get("POSTagListText"));
					System.out.println("Bigram: " + docen.get("Bigram"));
					System.out.println("Trigram: " + docen.get("Trigram"));
					System.out.println("Fourgram: " + docen.get("Fourgram"));

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
		}
	}
}
