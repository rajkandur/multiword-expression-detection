package parsing.ioparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Wiki50Reader {

	String m_wiki50FilePath = "";
	Set<String> m_SentMWEList = null; //mwe;word;sentence\r\n

	public Wiki50Reader() {
		m_wiki50FilePath = "./corpora/wiki50/IOB/wiki50_distilled.iob/";
		m_SentMWEList = new HashSet<String>();

		File mwefile = new File("./wikimwe.txt");
		if(!mwefile.exists()) return;

		FileInputStream in;
		try {
			in = new FileInputStream(mwefile);
			byte[] data = new byte[in.available()];
			in.read(data);
			String mwes = new String(data);
			m_SentMWEList.addAll(Arrays.asList(mwes.split(MWECorporaFileReader.SENTENCEDELIMITER)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public Set<String> getM_SentMWEList() {
		return m_SentMWEList;
	}

	private List<String> readwiki50corpus(){

		//Reading wiki50
		FileReader readeren = null;
		List<String> listofExpressions = null;

		try {
			listofExpressions = new ArrayList<String>();
			readeren = new FileReader(m_wiki50FilePath);
			BufferedReader reader = new BufferedReader(readeren);
			String text = null;
			int nLineCount = 0;
			while( (text = reader.readLine()) != null){

				nLineCount++;
				if(text.contains("-DOCSTART-") || text.trim().isEmpty() )
					continue;

				//split the space delim and put as semicolon delim into the list
				String[] textArray = text.split(" ");
				listofExpressions.add(textArray[0] + ";" + textArray[1]);

//				if(text.contains("assistant") || text.contains("coach"))
//					System.out.println(nLineCount + ": " + Arrays.asList(textArray));
			}
			System.out.println("Line Count:" + nLineCount);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listofExpressions;
	}

	/**
	 * 
	 * @param filepath
	 * @throws IOException
	 */
	public List<String> getwiki50Sentences() throws IOException
	{
		List<String> s = new ArrayList<String>();

		//reading wiki50 corpus
		List<String> wikiListText = readwiki50corpus();

		String enText = null;
		String mweText = null;
		String listtext = null;
		int nLineCount = 0;

		//get all the tokens in a line and store it as sentences
		Iterator<String> itr = wikiListText.iterator();
		String sOriginalText = "";
		String sMWETagText = "";
		String sOnlyOriginalText = "";
		String sOnlyMWETagText = "";
		boolean bMWEFound = false;
		boolean bNEFound = false;
		while( itr.hasNext() )
		{
			nLineCount++;
			listtext = itr.next();
			enText = listtext.split(";")[0];
			mweText = listtext.split(";")[1];

			if(enText.contentEquals(".")){//create a list of sentence
				sOriginalText = sOriginalText.trim() +  "."; 
				sMWETagText += "O";

				//creating sentence list and map
				s.add(sOriginalText);

//				if(!itr.hasNext()){
//					System.out.println(sOriginalText);
//					System.out.println(nLineCount);
//				}

				//clear the old values
				sOriginalText = "";
				sMWETagText = "";
			}
			else{ //construct a sentence
				if(enText.trim().contentEquals(",")){
					sOriginalText = sOriginalText.trim() + enText + " ";
					
					if(!bNEFound && mweText.contains("B-NE_")){
						bNEFound = true;
						if(bMWEFound){
							String mwepair = MWECorporaFileReader.SENTENCEDELIMITER + sOnlyMWETagText + ";" + sOnlyOriginalText;
							
							mwepair = mwepair.replace(",", "");
							if(!mwepair.trim().contentEquals(";")) 
								m_SentMWEList.add(mwepair);
							
							bMWEFound = false;
							
							//clear the old values
							sOnlyOriginalText = "";
							sOnlyMWETagText = "";
						}
					}
					
					else if(bNEFound && ( mweText.contains("B-MWE_") || ( mweText.contains("B-NE_")) ) ){
						String mwepair = MWECorporaFileReader.SENTENCEDELIMITER + sOnlyMWETagText + ";" + sOnlyOriginalText ;
						
						mwepair = mwepair.replace(",", "");
						if(!mwepair.trim().contentEquals(";")) 
							m_SentMWEList.add(mwepair);
						
						if( mweText.contains("B-NE_"))
							bNEFound = true;
						else
							bNEFound = false;
						
						bMWEFound = false;
						
						//clear the old values
						sOnlyOriginalText = "";
						sOnlyMWETagText = "";
					}
						
					//only text corresponding to mwe and not for 'O' or B-SENT_BOUND
					if(!mweText.trim().contentEquals("O") || !mweText.trim().contentEquals("B-SENT_BOUND"))
						sOnlyOriginalText = sOnlyOriginalText.trim() + enText + " ";
				}
				else{
					sOriginalText += enText + " ";
					
					if(!bNEFound && mweText.contains("B-NE_")){
						bNEFound = true;
						if(bMWEFound){
							String mwepair = MWECorporaFileReader.SENTENCEDELIMITER + sOnlyMWETagText + ";" + sOnlyOriginalText;
							
							mwepair = mwepair.replace(",", "");
							if(!mwepair.trim().contentEquals(";")) 
								m_SentMWEList.add(mwepair);
							
							bMWEFound = false;
							
							//clear the old values
							sOnlyOriginalText = "";
							sOnlyMWETagText = "";
						}
					}
					
					else if(bNEFound && ( mweText.contains("B-MWE_") || ( mweText.contains("B-NE_")) ) ){
						String mwepair = MWECorporaFileReader.SENTENCEDELIMITER + sOnlyMWETagText + ";" + sOnlyOriginalText;
						
						mwepair = mwepair.replace(",", "");
						if(!mwepair.trim().contentEquals(";")) 
							m_SentMWEList.add(mwepair);
						
						if( mweText.contains("B-NE_"))
							bNEFound = true;
						else
							bNEFound = false;
						
						bMWEFound = false;
						
						//clear the old values
						sOnlyOriginalText = "";
						sOnlyMWETagText = "";
					}
					
					//only text corresponding to mwe and not for 'O' or or B-SENT_BOUND
					if(!mweText.trim().contentEquals("O") && !mweText.trim().contentEquals("B-SENT_BOUND"))
						sOnlyOriginalText += enText + " ";
				}

				
				sMWETagText += mweText + " ";
				
				//only mwe no 'O' or or B-SENT_BOUND
				if(!mweText.trim().contentEquals("O") && !mweText.trim().contentEquals("B-SENT_BOUND"))
					sOnlyMWETagText += mweText + " ";

				//add all the mwes
				if( mweText.trim().contentEquals("O")){
					if(bMWEFound){
						String mwepair = MWECorporaFileReader.SENTENCEDELIMITER + sOnlyMWETagText + ";" + sOnlyOriginalText;
						
						mwepair = mwepair.replace(",", "");
						if(!mwepair.trim().contentEquals(";")) 
							m_SentMWEList.add(mwepair);
						
						bMWEFound = false;
						bNEFound = false;
						
						//clear the old values
						sOnlyOriginalText = "";
						sOnlyMWETagText = "";
					}	
				}
				else if(mweText.contains("I-MWE_") || mweText.contains("I-NE_")) bMWEFound = true;
			}
		}

		//write all mwes into a file
		FileOutputStream o = new FileOutputStream(new File("./mwe_wiki50.txt"));
		Iterator<String> mweitr = m_SentMWEList.iterator();
		while(mweitr.hasNext()) o.write(mweitr.next().getBytes());
		o.close();

		return s;
	}
}
