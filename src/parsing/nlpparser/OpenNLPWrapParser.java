package parsing.nlpparser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;

public class OpenNLPWrapParser {

	private InputStream m_tokenStream;
	private TokenizerModel m_tokenmodel;
	private TokenizerME m_tokenizer;

	private InputStream  m_posin;
	POSModel m_posmodel;
	POSTaggerME m_postagger;

	static OpenNLPWrapParser m_ONLPParser;

	private OpenNLPWrapParser() {
		try {

			//create a tokenizer
			m_tokenStream = new FileInputStream("./models/opennlp/en-token.bin");
			m_tokenmodel = new TokenizerModel(m_tokenStream);
			m_tokenizer =  new TokenizerME(m_tokenmodel);

			//create a postagger
			m_posin = new FileInputStream("./models/opennlp/en-pos-maxent.bin");
			m_posmodel = new POSModel(m_posin);
			m_postagger = new POSTaggerME(m_posmodel);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static OpenNLPWrapParser getInstance(){
		if( null == m_ONLPParser)
			m_ONLPParser = new OpenNLPWrapParser();	
		return m_ONLPParser;
	}

	public String[] tokenizeSentence(String s){
		String[] tokens = m_tokenizer.tokenize(s);
		return tokens;		
	}

	public String[] POStagSentence(String[] sent){
		return m_postagger.tag(sent);
	}

	private List<String> identifyPOStags(String[] tagsOfSentence, String[] tokenedSent, List<String> POStags){

		//extract the POS tags and generate the score from the table data
		List<Integer> POSIndexes = new ArrayList<Integer>();

		for(String POS: POStags){
			for(int i = 0; i < tagsOfSentence.length; i++){
				if( tagsOfSentence[i].contains(POS) )
					POSIndexes.add(i);
			}
		}

		//get all the noun forms in the sentence
		List<String> POSWords = new ArrayList<String>();
		for(int index: POSIndexes)
			POSWords.add(tokenedSent[index]);

		return POSWords;
	}

	public List<String> getPOSWordsofSentence(String sent, List<String> POStags){

		//tokenize and get the POS tags
		String[] tokenedSent = tokenizeSentence(sent);
		String[] tagsOfSentence = POStagSentence(tokenedSent);

		List<String> POSWords = identifyPOStags(tagsOfSentence, tokenedSent, POStags);
		return POSWords;
	}
}
