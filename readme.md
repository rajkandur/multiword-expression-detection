### MultiWord Detection System

The project contains 5 packages,

* _ml:_ This package contains MWEClassifier.java which contains the implemenation for learning algorithm

* _mwe:_ This package has ```MWEDetector.java, PatternMatcher.java and StatisticalAnalyzer.java```. MWEDetector has the main method for running the project.

* _parsing.ioparser:_ This package has three files MWECorporaFileReader.java which calls SemCorReader and Wiki50Reader class for respecting corpus reading and indexing.

* _ui:_ This package contains a ui but it is not integrated. :(

* _utils:_ This package contains all the utility classes which are used to generated positive and negative instances from the corpus.

#### Libraries

* weka.jar
* lucene-4.0.0
* opennlp-maxent-3.0.2-incubating.jar
* opennlp-tools-1.5.2-incubating.jar
* opennlp-uima-1.5.2-incubating.jar
* edu.mit.jsemcor_1.0.1_jdk.jar
* commons-lang3-3.1.jar